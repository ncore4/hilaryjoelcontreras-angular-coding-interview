import { Component, OnInit, OnDestroy } from '@angular/core';
import { QuestionsService } from '../../core/services/questions.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { QuestionModel } from '../../core/state/question.model';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-play',
  templateUrl: './play.component.html',
  styleUrls: ['./play.component.scss']
})
export class PlayComponent implements OnInit {

  corrects: number = 0;
  questionsTouched: number = 0;

  gameSubs!: Subscription;

  questions$ = this.questionsService.questions$;
  gettingQuestions$ = this.questionsService.gettingQuestions$;
  getQuestionsSubscription: Subscription = this.route.queryParams
    .pipe(switchMap(params =>
      this.questionsService.getQuestions({
        type: params.type,
        amount: params.amount,
        difficulty: params.difficulty
      })
    )).subscribe({
      error: () => {
        Swal.fire("The data cannot be fetched");
      }
    }
    );


  constructor(
    private readonly route: ActivatedRoute,
    private readonly questionsService: QuestionsService,
    private readonly router: Router
  ) { }

  ngOnInit(): void {
    this.gameSubscribe();
  }

  ngOnDestroy(): void {
    if (this.gameSubs) {
      this.gameSubs.unsubscribe();
    }
  }

  onAnswerClicked(questionId: QuestionModel['_id'], answerSelected: string): void {
    this.questionsService.selectAnswer(questionId, answerSelected);
  }


  gameSubscribe() {

    this.gameSubs = this.questions$.subscribe((questions) => {

      if (Array.isArray(questions) && questions.length === 0 || !questions) {
        return;
      }

      this.corrects = 0;
      this.questionsTouched = 0;

      questions.map(question => {
        let answer = question.answers.filter(choosen => choosen._id === question.selectedId)[0] || null;

        if (answer) {
          this.questionsTouched++;
        }

        if (answer && answer.isCorrect) {
          this.corrects++;
        }
      });

      if (this.questionsTouched === questions.length) {
        //happy message here

        Swal.fire(`Game Over, the score is ${this.corrects} answers of ${questions.length} questions`).then(() => {
          this.router.navigate(['/home']);
        })


      }



    })

  }

}
