import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlayRoutingModule } from './play-routing.module';
import { PlayComponent } from './play.component';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';


@NgModule({
  declarations: [
    PlayComponent,
  ],
  imports: [
    CommonModule,
    PlayRoutingModule,
    SweetAlert2Module
  ]
})
export class PlayModule { }
